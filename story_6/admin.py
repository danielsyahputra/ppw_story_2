from django.contrib import admin
from .models import Aktifitas, Member

# Register your models here.
admin.site.register(Aktifitas)
admin.site.register(Member)