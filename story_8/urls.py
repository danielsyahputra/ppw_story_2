from django.urls import path
from . import views

app_name= 'story_8'

urlpatterns = [
    path('', views.index, name='story_8'),
    path('search/<str:query>/', views.search_book, name='search'),
    path('post/', views.post_like_data, name='post')
]